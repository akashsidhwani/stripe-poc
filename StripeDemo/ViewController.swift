//
//  ViewController.swift
//  StripeDemo
//
//  Created by Akash Sidhwani on 17/05/22.
//

import UIKit
import Stripe


class ViewController: UIViewController {
    
    
    private var paymentIntentClientSecret: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /*
         Call api that would create a PaymentIntent
         In the response fetch "clientSecret" value and
         Initialise paymentIntentClientSecret variable
         eg. paymentIntentClientSecret = JSON["clientSecret"] as? String
         
         After this Enable Pay button to present Stripe UI for payment
         
         */
        
//        let params = ["patientId" : "d277d8b9-3464-4ebe-b456-b5faeed0feae",
//                      "amount": 10,
//                      "paymentOrderDescription": "Testing" ] as Dictionary<String, String>
//
//        var request = URLRequest(url: URL(string: "https://curapatient.dev.cc.curapatient.com/api/stripe/create-intent")!)
//        request.httpMethod = "POST"
//        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//
//        let session = URLSession.shared
//        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
//            print(response!)
//            do {
//                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
//                print(json)
//            } catch {
//                print("error")
//            }
//        })
//
//        task.resume()
        let parameters: [String: Any] = ["patientId" : "d277d8b9-3464-4ebe-b456-b5faeed0feae", "amount": "10", "paymentOrderDescription": "Testing" ]
          
          // create the url with URL
          let url = URL(string: "https://curapatient.dev.cc.curapatient.com/api/stripe/create-intent")! // change server url accordingly
          
          // create the session object
          let session = URLSession.shared
          
          // now create the URLRequest object using the url object
          var request = URLRequest(url: url)
          request.httpMethod = "POST" //set http method as POST
          
          // add headers for the request
          request.addValue("application/json", forHTTPHeaderField: "Content-Type") // change as per server requirements
          request.addValue("application/json", forHTTPHeaderField: "Accept")
          
          do {
            // convert parameters to Data and assign dictionary to httpBody of request
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
          } catch let error {
            print(error.localizedDescription)
            return
          }
          
          // create dataTask using the session object to send data to the server
          let task = session.dataTask(with: request) { data, response, error in
            
            if let error = error {
              print("Post Request Error: \(error.localizedDescription)")
              return
            }
            
            // ensure there is valid response code returned from this HTTP response
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode)
            else {
              print("Invalid Response received from the server")
              return
            }
            
            // ensure there is data returned
            guard let responseData = data else {
              print("nil Data received from the server")
              return
            }
            
            do {
              // create json object from data or use JSONDecoder to convert to Model stuct
              if let jsonResponse = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String: Any] {
                print(jsonResponse)
                // handle json response
              } else {
                print("data maybe corrupted or in wrong format")
                throw URLError(.badServerResponse)
              }
            } catch let error {
              print(error.localizedDescription)
            }
          }
          // perform the task
          task.resume()
    }
    
    // THESE Functions are for Integrated UI and payment
    @IBAction func payTapped(_ sender: Any) {
        
        
        var configuration = PaymentSheet.Configuration()
               configuration.merchantDisplayName = "Covid Clinic"
        
        // Enables Apple Pay 
               configuration.applePay = .init(
                   merchantId: "com.example.appname",
                   merchantCountryCode: "US"
               )

               let paymentSheet = PaymentSheet(
                paymentIntentClientSecret: paymentIntentClientSecret ?? "",
                   configuration: configuration)

               paymentSheet.present(from: self) { [weak self] (paymentResult) in
                   switch paymentResult {
                   case .completed:
                       print("Payment complete!")
                   case .canceled:
                       print("Payment canceled!")
                   case .failed(let error):
                       print(error.localizedDescription)
                   }
               }
    }
}

